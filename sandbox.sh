ThisOS()
{
	echo $( cat /etc/os-release \
		| sed -n "s/^ID=\(.*\)$/\1/p" \
		| head -n1 )
}
ThisOSVersion()
{
	full_version=`cat /etc/os-release | sed -n 's/^VERSION=\"\([0-9][0-9\.]*\)[ \t]*.*\"$/\1/p'`
	IFS="\." read -ra ver <<< $full_version
	if [ ! -z "$1" ]; then
		# $1 = 0 : Major Version
		# $1 = 1 : Minor Version
		# $1 = 2 : Patch
		echo ${ver[$1]}
	else
		echo $full_version
	fi
}
WhoMade()
{
	local hardware=$1
	echo $( lshw -class $hardware \
	| sed -n 's/.*vendor\:[ \t]*\(.*\)[ \t]*/\1/p' \
	| head -n1 )
}



GetMostRecent()
{
	local base_name=$1
	local list="$2"
	local editions=()

	
	IFS=" " read -ra ver <<< $list
	for i in ${ver[@]}; do
		editions+=$( echo $i \
			| sed -n "s/$base_name\([0-9][0-9]*\)[\ \t]*.*$/\1/p" )" "
	done
	IFS=$'\n' echo "${editions[*]}" \
		| tr " " "\n" \
		| sort -nr \
		| head -n1
}


GetAMD()
{
	local os_flavor=$1
	echo "Getting AMD's drivers..."

	case $os_flavor in
		"ubuntu")
			lts_version=16
			if [ $mjr_version -ge $lts_version ]; then
				echo "Modern version of Ubuntu"
				#amdgpu-pro

			else
				echo "Older version of Ubuntu"
				#fglrx

			fi
			;;
		"fedora")
			;;
		*)
			;;
	esac
	echo "NVIDIA Devices: "
}
GetNVIDIA()
{
	local os_flavor=$1
	echo "Getting NVIDIA's drivers..."

	case $os_flavor in
		"ubuntu")
			local app="nvidia-"
			local app_list=$(sudo apt-cache search -qq "$app" | sed -n "s/[ ].*//p")
			echo "Installing: $app"`GetMostRecent $app "$app_list"`"*"
			apt install -qq -fy "$app"`GetMostRecent $app "$app_list"`"*"
			apt-get autoremove -qq -y
			;;
		"fedora")
			;;
		*)
			;;
	esac
	echo "NVIDIA Devices: "
	local gpus=$(nvidia-smi -L \
		| sed -n "s/.*:[ ]*\(.*\)[ ]*(.*/\1/p")
	echo $gpus
}

Uses()
{
	local check=$1
	local manufacturers=$2
	[ ! -z `echo $manufacturers | grep -Poi "$check"` ] && echo 1 || echo 0
}


UpdateGraphics()
{
	local os_flavor=`ThisOS`
	echo "$os_flavor: `ThisOSVersion`"
	local lts_version=0

	local mjr_version=`ThisOSVersion 0`
	local graphics_manufacturer=`WhoMade "display"`	
	local cpu_manufacturer=`WhoMade "cpu"`

	echo "CPU drivers required from: $cpu_manufacturer"
	echo "Graphics drivers required from: $graphics_manufacturer"

	if [ `Uses "nvidia" $graphics_manufacturer` ]; then
		GetNVIDIA $os_flavor

	elif [ `Uses "amd" $graphics_manufacturer` ]; then
		GetAMD $os_flavor

#	elif [ `Uses "intel" $graphics_manufacturer` ]; then
#		GetIntel $os_flavor
#
#	elif [ `Uses "arm" $graphics_manufacturer` ]; then
#		GetARM $os_flavor
#
	else
		echo "Non-Standard: $graphics_manufacturer"

	fi
}


clear

#echo "This Operating System: `ThisOS`"
#echo "This Operating System's Version: `ThisOSVersion`"
#echo "This Operating System's Major Version: `ThisOSVersion 0`"
#echo "This Operating System's Minor Version: `ThisOSVersion 1`"
#echo "This Operating System's Patch: `ThisOSVersion 2`"

#cpu_manufacturer=`WhoMade "cpu"`
#graphics_manufacturer=`WhoMade "display"`
#echo "CPU drivers required from: $cpu_manufacturer"
#echo "Graphics drivers required from: $graphics_manufacturer"

UpdateGraphics
