#!bin/bash
dl_from="http://vulkan.gpuinfo.org/displayreport.php?id="
dl_to="$HOME/Documents/new/"
record="device"
grep_exp="deviceid"
padd=5
tolerance=150
verbosity=1

initial_index=0
#initial_index=2375

Verbose() { if [ $1 -le $2 ]; then echo "$3"; fi }

PaddZero() { local buff="$2"; printf $( echo "%0"$buff"d" ) $1; }

AssertDir() { if [ ! -d $1 ]; then mkdir $1; chmod 777 $1; fi }

DownloadDynamicURL()
{
	dl_to=`AssertDir $dl_to`
	declare -i blanks=0
	declare -i iter=$initial_index
	item=$( wget -q "$dl_from$iter" -O - | grep -Pi "$grep_exp" )
	while [ $blanks -le $tolerance ]; do
		ident=`PaddZero $iter $padd`
		filename="$record$ident"
		filepath="$dl_to$filename"
		if [ ! -f "$filepath" ]; then
			if [ ! -z "$item" ]; then
				Verbose 1 $verbosity "downloading $filename..."
				wget -qN "$dl_from$iter" -O "$filepath"
				chmod 777 "$filepath"
				blanks=0
			else
				blanks+=1
				Verbose 1 $verbosity "$filename does not exist.	Blanks: $blanks"
			fi
		else
			Verbose 1 $verbosity "$filename already downloaded. Checking for updates..."
			wget -qN "$dl_from$iter" -O "$filepath"
			chmod 777 "$filepath"
			blanks=0
		fi
		iter+=1
		item=$( wget -q "$dl_from$iter" -O - | grep -Pi "$grep_exp" )
	done
}

DownloadDynamicURL
echo "Device Count: `ls -l $dl_to$record* | wc -l`"
